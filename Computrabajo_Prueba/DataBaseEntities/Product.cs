﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Computrabajo_Prueba.DataBaseEntities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string Category { get; set; }
    }
}