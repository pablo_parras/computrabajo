﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using Computrabajo_Prueba.Helpers;
using System.IO;

namespace Computrabajo_Prueba.DataBaseEntities
{
    public class Context
    {
        private readonly MySqlConnection Conector;

        public Context()
        {
            Conector = new MySqlConnection(ConfigurationManager.ConnectionStrings["Products"].ConnectionString);
            Conector.Open();
            try
            {
                if (!CheckExistDatabase())
                {
                    RestoreBehavior();
                }

                Conector.Close();
                Conector = new MySqlConnection(ConfigurationManager.ConnectionStrings["Products"].ConnectionString + "; database = Products");
                Conector.Open();
            }
            catch (Exception e)
            {
                throw new Exception("Error to conect and create database", e);
            }
        }

        public async Task<List<T>> CreateCommandList<T>() where T : class, new()
        {
            var query = $"Select * from {typeof(T).Name}";
            var resultList = new List<T>();

            var command = new MySqlCommand(query, Conector);
            var result = await command.ExecuteReaderAsync();

            while (await result.ReadAsync())
            {
                var obj = new T();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        var propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(result[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                resultList.Add(obj);
            }

            command.Dispose();
            result.Close();

            return resultList;
        }

        /// <summary>
        /// Select without subquerys and without relations tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propoerties"></param>
        /// <returns></returns>
        public async Task<List<T>> GetListByParameters<T>(Dictionary<string, string> infoValues) where T : class, new()
        {
            var resultList = new List<T>();

            string filters = string.Join(" and ", infoValues.Select(x => x.Key + " like " + x.Value).ToArray());
            var query = $"Select * from {typeof(T).Name} where {filters}";

            var command = new MySqlCommand(query, Conector);
            var result = await command.ExecuteReaderAsync();

            while (await result.ReadAsync())
            {
                var obj = new T();
                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        var propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(result[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }

                resultList.Add(obj);
            }

            command.Dispose();
            result.Close();

            return resultList;
        }

        public async Task<T> FirstOrDefaultById<T>(int id) where T : class, new()
        {
            var command = new MySqlCommand($"Select * from {typeof(T).Name} where Id = {id}", Conector);
            var result = await command.ExecuteReaderAsync();

            var obj = new T();

            while (await result.ReadAsync())
            {
                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        var propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(result[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            command.Dispose();
            result.Close();

            return obj;
        }

        public async Task<T> Insert<T>(T obj) where T : class, new()
        {
            var queryGetMaxId = $"select Max(Id) from {typeof(T).Name}";
            var commandGetMaxId = new MySqlCommand(queryGetMaxId, Conector);
            var resultGetMaxId = await commandGetMaxId.ExecuteReaderAsync();

            var maxId = string.Empty;

            while (await resultGetMaxId.ReadAsync())
            {
                maxId = ((int)resultGetMaxId["Max(Id)"] + 1).ToString();
            }

            commandGetMaxId.Dispose();
            resultGetMaxId.Close();

            var propertyInfoId = obj.GetType().GetProperty("Id");
            obj.GetType().GetProperty("Id").SetValue(obj, Convert.ChangeType(maxId, propertyInfoId.PropertyType), null);

            var query = $"Insert into {typeof(T).Name} " +
                $"({string.Empty.ConvertClassToFieldsQuery(obj)}) " +
                $"values ({string.Empty.ConvertClassToValuesQuery(obj)})";

            var command = new MySqlCommand(query, Conector);
            var result = await command.ExecuteReaderAsync();

            return obj;
        }

        public async Task<T> Update<T>(T obj, int id) where T : class, new()
        {
            var entity = await FirstOrDefaultById<T>(id);

            if (entity == null || entity.GetType().GetProperty("Id").GetValue(entity).ToString() == "0")
                throw new KeyNotFoundException(
                    $"Error to update table {typeof(T).Name} with Id: {id.ToString()}"
                    );

            var keysValues = new Dictionary<string, string>();

            foreach (var prop in obj.GetType().GetProperties())
            {
                var value = prop.GetValue(obj)?.ToString();

                if (prop.GetValue(obj).GetType() == typeof(string))
                {
                    value = "'" + value + "'";
                }

                else value = value.Replace(',', '.');

                keysValues.Add(prop.Name, value);

            }

            var valuesQuery = new List<string>();

            foreach (var keyValue in keysValues)
            {
                valuesQuery.Add($"{keyValue.Key} = {keyValue.Value}");
            }

            var query = $"Update {typeof(T).Name} set " +
                $"{string.Join(",", valuesQuery)} " +
                $"where Id = {id.ToString()}";

            var command = new MySqlCommand(query, Conector);
            var result = await command.ExecuteReaderAsync();

            command.Dispose();
            result.Close();

            return obj;
        }

        private bool CheckExistDatabase()
        {
            var result = false;
            var createDatabase = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'Products'";
            var cmd = new MySqlCommand(createDatabase, Conector);

            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                int count = reader.GetInt32(0);
                if (count == 1)
                {
                    result = true;
                }
            }
            cmd.Dispose();
            reader.Close();

            return result;
        }

        private void RestoreBehavior()
        {
            string constring = ConfigurationManager.ConnectionStrings["Products"].ConnectionString;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "backup.sql");
            using (MySqlConnection conn = new MySqlConnection(constring))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    using (var mb = new MySqlBackup(cmd))
                    {
                        cmd.Connection = conn;
                        conn.Open();
                        mb.ImportInfo.TargetDatabase = "Products";/*Here,How can I check here if mydb exist then skip the code?*/
                        mb.ImportInfo.DatabaseDefaultCharSet = "utf8";
                        mb.ImportFromFile(file);
                    }
                }
            }
        }
    }
}