﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Computrabajo_Prueba.Helpers
{
    public class InfoValue
    {
        private string Name { get; }
        private string Type { get; }
        private string Value { get; }

        public InfoValue(string name, string type, string value)
        {
            Name = name;
            Type = type;
            Value =  value;
        }
    }
}