﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Computrabajo_Prueba.Helpers
{
    public static class StringHelper
    {
        public static IEnumerable<string> ConvertClassToFields<T>(this string query, T obj) where T : class
        {
            return obj.GetType().GetProperties().OrderBy(x => x.Name).Where(p =>
               obj.GetType().GetProperty(p.Name) != null
                ).Select(x => x.Name);
        }

        public static string ConvertClassToFieldsQuery<T>(this string query, T obj) where T : class, new()
        {
            var fields = query.ConvertClassToFields(obj);

            return string.Join(",", fields);
        }

        public static IEnumerable<string> ConvertClassToValues<T>(this string query, T obj) where T : class
        {
            return obj.GetType().GetProperties().OrderBy(x => x.Name).Where(p =>
               obj.GetType().GetProperty(p.Name) != null
                ).Select(x => {

                    var value = x.GetValue(obj)?.ToString();

                    if (x.GetValue(obj).GetType() == typeof(string))
                    {
                        value = "'" + value + "'";
                    }
                    else value = value.Replace(',', '.');

                    return value;
                });
        }

        public static string ConvertClassToValuesQuery<T>(this string query, T obj) where T : class
        {
            var values = query.ConvertClassToValues(obj);

            return string.Join(",", values);
        }
    }
}