﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Computrabajo_Prueba.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public short CategoryId { get; set; }
    }
}