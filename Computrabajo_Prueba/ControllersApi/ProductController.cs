﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Computrabajo_Prueba.DataBaseEntities;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;

namespace Computrabajo_Prueba.ControllersApi
{
    public class ProductController : ApiController
    {
        private readonly Context _context;

        public ProductController()
        {
            _context = new Context();
        }
        // GET api/<controller>
        [HttpGet]
        [Route("api/Products")]
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await _context.CreateCommandList<Product>();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/<controller>/5
        [HttpGet]
        [Route("api/products/{id:int}")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                var response = new HttpResponseMessage();

                var cookie = new CookieHeaderValue("session-id", "12345");
                cookie.Expires = DateTimeOffset.Now.AddDays(1);
                cookie.Domain = Request.RequestUri.Host;
                cookie.Path = "/";
                response.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                
                var result = await _context.FirstOrDefaultById<Product>(id);

                response.Content = new ObjectContent(typeof(Product), result, new JsonMediaTypeFormatter());

                return Ok(response);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET api/<controller>/Targetas/Asus
        // Example with Cookies
        [HttpGet]
        [Route("api/products")]
        public async Task<IHttpActionResult> GetByCategoryAndName([FromUri] string category, [FromUri] string name)
        {
            try
            {
                var response = new HttpResponseMessage();

                var cookie = new CookieHeaderValue("session-id", "12345");
                cookie.Expires = DateTimeOffset.Now.AddDays(1);
                cookie.Domain = Request.RequestUri.Host;
                cookie.Path = "/";
                response.Headers.AddCookies(new CookieHeaderValue[] { cookie });

                var keysValues = new Dictionary<string, string>();

                //if value doesnt string not add "'%" to start and end
                keysValues.Add(nameof(category), $"'%{category}%'");
                keysValues.Add(nameof(name), $"'%{name}%'");

                var result = await _context.GetListByParameters<Product>(keysValues);

                response.Content = new ObjectContent(typeof(Product), result, new JsonMediaTypeFormatter());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /*
         * User-Agent: Fiddler
         * Host: localhost:51448
         * Content-Length: 115
         * Content-type: application/json
         * */
        // POST api/<controller>
        [HttpPost]
        [Route("api/Products")]
        public async Task<IHttpActionResult> Post([FromBody]Product value)
        {
            try
            {
                await _context.Insert(value);
                return Ok(HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message); Ok(HttpStatusCode.InternalServerError);
            }
        }

        // PUT api/<controller>/5
        [HttpPut]
        [Route("api/Products/{id:int}")]
        public async Task<IHttpActionResult> Put([FromBody]Product value, int id)
        {
            try
            {
                await _context.Update(value, id);
                return Ok(value);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}