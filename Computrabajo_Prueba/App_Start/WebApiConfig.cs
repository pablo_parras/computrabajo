﻿using Computrabajo_Prueba.DataBaseEntities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Computrabajo_Prueba
{
    public static class WebApiConfig
    {
        private static IServiceCollection serviceCollection;

        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            //ConfigureServices(serviceCollection);
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddIOC();
        }
    }
}
