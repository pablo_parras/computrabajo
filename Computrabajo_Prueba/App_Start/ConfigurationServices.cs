﻿using Computrabajo_Prueba.DataBaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ConfigurationServices
    {
        public static IServiceCollection AddIOC(this IServiceCollection services)
        {

            services.AddScoped<Context>();

            return services;
        }
    }
}