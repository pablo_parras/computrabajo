create database products;
use Products;

create table Product(
Id int primary key,
Name varchar(100),
Description  varchar (100),
Price double,
Category varchar (100)
);

insert into product values (1, 'Placa base', 'Placa base', 10.30, 'Placa base');
insert into product values (2, 'Procesadores', 'Procesadores', 10.30, 'Procesadores');
insert into product values (3, 'Disco duros', 'Disco duros', 10.30, 'Disco duros');
insert into product values (4, 'Sapphire Radeon RX Vega 64 8GB HBM2', 'Las Sapphire Radeon RX Vega 64 montan...', 10.30, 'Tarjetas Gráficas');
insert into product values (5, 'Memoria RAM', 'Memoria RAM', 10.30, 'Memoria RAM');
insert into product values (6, 'Carcasas', 'Carcasas', 10.30, 'Carcasas');